## Swag Labs Demo QA

Swag Labs Demo Selenide Automation

> Java 11 is required to run tests

### Setup docker

- `docker pull selenoid/vnc_chrome:111.0`
- `docker pull selenoid/video-recorder:latest`
- `docker-compose up` (run in bash)
- open http://localhost:8082/#/

### Install project dependencies

- `mvn clean install -DskipTests`

### Run tests

- `mvn clean test -Dtest=TestRunner -DfailIfNoTests=false`

### Generate report

- `mvn allure:serve`

</br><p align="center">
<img src="report-1.png"/></br>
<img src="report-2.png"/></br>
