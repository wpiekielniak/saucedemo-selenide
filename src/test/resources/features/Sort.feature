@store
Feature: Sorting
  As a User I want to sort products by name and price

  Background:
    Given I'm on the login page
    When I log in as standard user
    Then I'm on the store page
    And I see list of products

  Scenario: Sort products by name
    When I sort products by name in asc order
    Then I see first product with name "Sauce Labs Backpack"

  Scenario: Sort products by price
    When I sort products by price in desc order
    Then I see first product with price greater than last product