@store
Feature: Login
  As a User I want to log in to the store

  Background:
    Given I'm on the login page

  Scenario: Log in with correct credentials
    When I log in as standard user
    Then I'm on the store page
    And I see list of products

  Scenario: Log in with incorrect credentials
    When I log in as locked out user
    Then I see error login message