package com.saucedemo.steps;

import com.saucedemo.page.CheckoutCompletePage;
import com.saucedemo.page.CheckoutPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

import static com.saucedemo.page.BasePage.verifyElementShouldBeVisible;
import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutSteps {

  private final CheckoutPage checkoutPage = new CheckoutPage();
  private final CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();

  @And("I fill checkout info:")
  public void iFillCheckoutInfo(DataTable table) {
    List<Map<String, String>> rows = table.asMaps(String.class, String.class);
    Map<String, String> columns = rows.get(0);
    checkoutPage.fillCheckoutInfo(
        columns.get("firstName"), columns.get("lastName"), columns.get("postalCode"));
  }

  @And("I go to checkout overview page")
  public void iGoToCheckoutOverviewPage() {
    checkoutPage.continueButton.click();
  }

  @And("I should see summary info")
  public void iShouldSeeSummaryInfo() {
    verifyElementShouldBeVisible(checkoutPage.summaryInfo);
  }

  @When("I finish checkout")
  public void iFinishCheckout() {
    checkoutPage.finishButton.click();
  }

  @Then("I should be redirected to checkout complete page")
  public void iShouldBeRedirectedToCheckoutCompletePage() {
    verifyElementShouldBeVisible(checkoutCompletePage.checkoutCompleteContainer);
    assertThat(checkoutCompletePage.getCurrentUrl()).contains("/checkout-complete");
  }
}
