package com.saucedemo.steps;

import com.saucedemo.page.CartPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import static com.saucedemo.page.BasePage.verifyElementShouldBeVisible;

public class CartSteps {

  private final CartPage cartPage = new CartPage();

  @And("I see cart is not empty")
  public void iSeeCartIsNotEmpty() {
    verifyElementShouldBeVisible(cartPage.cartItem);
  }

  @Then("I go to checkout page")
  public void iGoToCheckoutPage() {
    cartPage.checkoutButton.click();
  }
}
