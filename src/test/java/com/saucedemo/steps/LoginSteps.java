package com.saucedemo.steps;

import com.codeborne.selenide.WebDriverRunner;
import com.saucedemo.page.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.saucedemo.entity.UserFactory.lockedUser;
import static com.saucedemo.entity.UserFactory.standardUser;
import static com.saucedemo.page.BasePage.verifyElementShouldBeVisible;

public class LoginSteps {

  private final LoginPage loginPage = new LoginPage();

  @Given("I go to login page")
  public void iGoToLoginPage() {
    WebDriverRunner.clearBrowserCache();
    loginPage.visit();
  }

  @Given("I'm on the login page")
  public void iMOnTheLoginPage() {
    iGoToLoginPage();
    verifyElementShouldBeVisible(loginPage.loginForm);
  }

  @Then("I see error login message")
  public void iSeeErrorLoginMessage() {
    verifyElementShouldBeVisible(loginPage.errorMessage);
    verifyElementShouldBeVisible(loginPage.loginLogo);
  }

  @When("I log in as standard user")
  public void iLogInAsStandardUser() {
    loginPage.loginAs(standardUser);
  }

  @When("I log in as locked out user")
  public void iLogInAsLockedOutUser() {
    loginPage.loginAs(lockedUser);
  }
}
