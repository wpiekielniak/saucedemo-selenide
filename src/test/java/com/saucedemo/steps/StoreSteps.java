package com.saucedemo.steps;

import com.codeborne.selenide.ElementsCollection;
import com.saucedemo.page.StorePage;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.text.NumberFormat;

import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.saucedemo.page.BasePage.verifyElementShouldBeVisible;

public class StoreSteps {

  private final StorePage storePage = new StorePage();

  @Then("I'm on the store page")
  public void iMOnTheStorePage() {
    verifyElementShouldBeVisible(storePage.shoppingCart);
  }

  @And("I see list of products")
  public void iSeeListOfProducts() {
    storePage.productsListElement.shouldNotBe(empty);
  }

  @When("I sort products by price in {order} order")
  public void iSortProductsByPriceInOrder(String orderBy) throws Exception {
    storePage.sortProductsByPrice(orderBy);
  }

  @When("I sort products by name in {order} order")
  public void iSortProductsByNameInAscOrder(String orderBy) throws Exception {
    storePage.sortProductsByName(orderBy);
  }

  @ParameterType("asc|desc")
  public String order(String value) {
    return value;
  }

  @Then("I see first product with price {priceStatus} than last product")
  public void iSeeFirstProductWithPriceThanLastProduct(String priceStatus) throws Exception {
    ElementsCollection inventoryPrices =
        storePage.productsListElement.$$(storePage.PRODUCT_PRICE_CSS);
    NumberFormat format = NumberFormat.getCurrencyInstance();
    double firstProductPrice =
        Double.parseDouble(String.valueOf(format.parse(inventoryPrices.texts().get(0))));
    double lastProductPrice =
        Double.parseDouble(
            String.valueOf(
                format.parse(inventoryPrices.texts().get(inventoryPrices.texts().size() - 1))));
    if (priceStatus.equals("greater")) {
      Assert.assertTrue(firstProductPrice > lastProductPrice);
    } else if (priceStatus.equals("less")) {
      Assert.assertTrue(firstProductPrice < lastProductPrice);
    } else {
      throw new Exception("Price status undefined!");
    }
  }

  @ParameterType("greater|less")
  public String priceStatus(String value) {
    return value;
  }

  @When("I add random product to cart")
  public void iAddRandomProductToCart() {
    storePage.addRandomProductToCart();
  }

  @And("I should see cart badge")
  public void iShouldSeeCartBadge() {
    verifyElementShouldBeVisible(storePage.shoppingCartBadge);
  }

  @And("I go to cart page")
  public void iGoToCartPage() {
    storePage.shoppingCartLink.click();
  }

  @Then("I see first product with name {string}")
  public void iSeeFirstProductWithName(String productName) {
    verifyElementShouldBeVisible($(storePage.PRODUCT_NAME_CSS));
    Assert.assertTrue($$(storePage.PRODUCT_NAME_CSS).texts().contains(productName));
  }
}
