package com.saucedemo;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.qameta.allure.selenide.AllureSelenide;
import io.qameta.allure.selenide.LogType;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.logging.Level;

@RunWith(Cucumber.class)
@CucumberOptions(
    strict = true,
    features = "classpath:features",
    tags = "not @ignore and @store",
    glue = {"com.saucedemo.steps", "com.saucedemo.config"},
    plugin = {
      "pretty",
      "json:target/cucumber.json",
      "io.qameta.allure.cucumber5jvm.AllureCucumber5Jvm"
    })
public class TestRunner {

  @BeforeSuite
  public static void setUp() {
    Configuration.fastSetValue = false;
    SelenideLogger.addListener(
        "AllureSelenide",
        new AllureSelenide()
            .screenshots(true)
            .savePageSource(true)
            .enableLogs(LogType.BROWSER, Level.ALL));
  }

  @AfterSuite
  public static void tearDown() {
    WebDriverRunner.closeWebDriver();
    WebDriverRunner.clearBrowserCache();
  }
}
