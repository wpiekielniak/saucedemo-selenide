package com.saucedemo.page;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.saucedemo.entity.UserFactory;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.saucedemo.config.ConfigFactory.config;

public abstract class BasePage {

  public static void verifyElementShouldBeVisible(SelenideElement element) {
    element.shouldBe(visible);
  }

  public static void verifyElementShouldHaveText(SelenideElement element, String expectedText) {
    element.shouldHave(text(expectedText));
  }

  public void visit() {
    UserFactory.init(config());
    open(config().baseUrl());
  }

  public String getCurrentUrl() {
    return WebDriverRunner.getWebDriver().getCurrentUrl();
  }
}
