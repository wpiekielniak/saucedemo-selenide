package com.saucedemo.page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;

public class StorePage extends BasePage {

  public final String PRODUCT_PRICE_CSS = ".inventory_item_price";
  public final String ADD_TO_CART_BUTTON_CSS = "button[id*=\"add-to-cart\"]";
  public final String PRODUCT_NAME_CSS = ".inventory_item_name";

  public final SelenideElement shoppingCart = $("#shopping_cart_container");
  public final SelenideElement addToCartButton = $(ADD_TO_CART_BUTTON_CSS);
  public final SelenideElement productsListElement = $(".inventory_list");
  public final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
  public final SelenideElement shoppingCartLink = $(".shopping_cart_link");
  public final SelenideElement productSort = $(".product_sort_container");
  public final SelenideElement productPrice = $(PRODUCT_PRICE_CSS);

  @Step("Adding random product to cart")
  public StorePage addRandomProductToCart() {
    Random rand = new Random();
    ElementsCollection addToCartButtons = productsListElement.$$(ADD_TO_CART_BUTTON_CSS);
    addToCartButtons.get(rand.nextInt(addToCartButtons.size())).click();
    return this;
  }

  @Step("Sorting products by name in {0} order")
  public StorePage sortProductsByName(String orderBy) throws Exception {
    if (orderBy.equals("asc")) {
      productSort.selectOptionByValue("az");
    } else if (orderBy.equals("desc")) {
      productSort.selectOptionByValue("za");
    } else {
      throw new Exception("Sorting undefined!");
    }
    return this;
  }

  @Step("Sorting products by price in {0} order")
  public StorePage sortProductsByPrice(String orderBy) throws Exception {
    if (orderBy.equals("asc")) {
      productSort.selectOptionByValue("lohi");
    } else if (orderBy.equals("desc")) {
      productSort.selectOptionByValue("hilo");
    } else {
      throw new Exception("Sorting undefined!");
    }
    return this;
  }
}
