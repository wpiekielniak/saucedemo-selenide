package com.saucedemo.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage extends BasePage {

  public final SelenideElement cartItem = $(".cart_item");
  public final SelenideElement checkoutButton = $("#checkout");
}
