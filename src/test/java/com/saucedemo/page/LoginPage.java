package com.saucedemo.page;

import com.codeborne.selenide.SelenideElement;
import com.saucedemo.entity.User;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage {

  public final SelenideElement loginLogo = $(".login_logo");
  public final SelenideElement usernameInput = $("#user-name");
  public final SelenideElement passwordInput = $("#password");
  public final SelenideElement loginButton = $("#login-button");
  public final SelenideElement errorMessage = $(".error-message-container");
  public final SelenideElement loginForm = $(".login_wrapper-inner");

  @Step("Login as {0}")
  public LoginPage loginAs(User user) {
    usernameInput.sendKeys(user.getUsername());
    passwordInput.sendKeys(user.getPassword());
    loginButton.click();
    return this;
  }
}
