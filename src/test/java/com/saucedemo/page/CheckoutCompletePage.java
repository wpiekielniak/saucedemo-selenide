package com.saucedemo.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutCompletePage extends BasePage {

  public final SelenideElement checkoutCompleteContainer = $("#checkout_complete_container");
}
