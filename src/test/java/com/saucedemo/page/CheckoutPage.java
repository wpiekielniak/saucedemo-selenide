package com.saucedemo.page;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage extends BasePage {

  public final SelenideElement firstNameInput = $("#first-name");
  public final SelenideElement lastNameInput = $("#last-name");
  public final SelenideElement postalCodeInput = $("#postal-code");
  public final SelenideElement continueButton = $("#continue");
  public final SelenideElement summaryInfo = $(".summary_info");
  public final SelenideElement finishButton = $("#finish");

  @Step("Filling checkout info")
  public CheckoutPage fillCheckoutInfo(String firstName, String lastName, String postalCode) {
    firstNameInput.sendKeys(firstName);
    lastNameInput.sendKeys(lastName);
    postalCodeInput.sendKeys(postalCode);
    return this;
  }
}
