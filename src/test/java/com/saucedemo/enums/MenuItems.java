package com.saucedemo.enums;

import lombok.Getter;

public enum MenuItems {
  ALL_ITEMS("All Items"),
  ABOUT("About"),
  LOGOUT("Logout"),
  RESET_APP_STATE("Reset App State");

  @Getter private final String value;

  MenuItems(String value) {
    this.value = value;
  }
}
