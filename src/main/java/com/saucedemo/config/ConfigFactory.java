package com.saucedemo.config;

import org.aeonbits.owner.ConfigCache;

public class ConfigFactory {
  public static ConfigProperties config() {
    return ConfigCache.getOrCreate(ConfigProperties.class);
  }
}
