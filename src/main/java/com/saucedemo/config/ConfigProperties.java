package com.saucedemo.config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({"system:env", "system:properties", "classpath:${env}/config.properties"})
public interface ConfigProperties extends Config {
  @Key("base.url")
  String baseUrl();

  @Key("standard-user.username")
  String standardUserUsername();

  @Key("standard-user.password")
  String standardUserPassword();

  @Key("locked-user.username")
  String lockedUserUsername();

  @Key("locked-user.password")
  String lockedUserPassword();
}
