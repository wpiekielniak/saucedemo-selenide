package com.saucedemo.entity;

import com.saucedemo.config.ConfigProperties;

public class UserFactory {
  public static User standardUser;
  public static User lockedUser;

  public static void init(ConfigProperties config) {
    standardUser = new User(config.standardUserUsername(), config.standardUserPassword());
    lockedUser = new User(config.lockedUserUsername(), config.lockedUserPassword());
  }
}
