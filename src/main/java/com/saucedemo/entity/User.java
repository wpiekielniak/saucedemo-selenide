package com.saucedemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "password")
@AllArgsConstructor
public class User {
  private String username;
  private String password;
}
